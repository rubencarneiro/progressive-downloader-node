"use strict";

/*
 * Copyright (C) 2020 UBports Foundation <info@ubports.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import path from "path";
import fs from "fs-extra";

import moxios from "moxios";
import sinon from "sinon";
import chai from "chai";
import sinonChai from "sinon-chai";
chai.use(sinonChai);
const expect = chai.expect;

import { checkFile, downloadOne, download } from "../../src/module.js";
import { execSync } from "child_process";

const testfile = path.resolve("tests/test-data/testfile");
const testtarget_1 = path.resolve("tests/test-data/targets/testtarget_1");
const testtarget_2 = path.resolve("tests/test-data/targets/testtarget_2");
const testtarget_3 = path.resolve("tests/test-data/targets/testtarget_3");
const sum = "ef537f25c895bfa782526529a9b63d97aa631564d5d789c2b765448c8635fb6c";

beforeEach(function() {
  // import and pass your custom axios instance to this method
  moxios.install();
});
afterEach(function() {
  // import and pass your custom axios instance to this method
  moxios.uninstall();
});

describe("Unit tests", function() {
  describe("checkFile()", function() {
    it("should resolve true if file exists and was verified", function() {
      return checkFile({
        path: testfile,
        checksum: {
          algorithm: "sha256",
          sum
        }
      }).then(ok => {
        expect(ok).to.eql(true);
      });
    });
    it("should resolve false if file exists but does not match", function() {
      return checkFile({
        path: testfile,
        checksum: { algorithm: "sha256", sum: "wrongsum" }
      }).then(ok => {
        expect(ok).to.eql(false);
      });
    });
    it("should resolve false if file does not exist", function() {
      return checkFile({
        path: path.join(testfile, "/what"),
        checksum: {
          algorithm: "sha256",
          sum
        }
      }).then(ok => {
        expect(ok).to.eql(false);
      });
    });
    [
      [{ path: testfile }, true],
      [{ path: testfile }, false],
      [{ path: testfile }]
    ].forEach(args =>
      it(`should resolve ${args[1] || false} if no checksum is specified and ${
        args[1]
      } is the second argument`, function() {
        return checkFile(...args).then(ok => {
          expect(ok).to.eql(args[1] || false);
        });
      })
    );
    it("should reject on error", function() {
      return checkFile({
        path: testfile,
        checksum: { algorithm: "what", sum: "???" }
      }).catch(error => {
        expect(error).to.eql(exist);
      });
    });
  });
  describe("downloadOne()", function() {
    it("should download a file", function(done) {
      const thenSpy = sinon.spy();
      const catchSpy = sinon.fake((...e) => console.log(...e));
      const progressSpy = sinon.spy();
      const nextSpy = sinon.spy();
      const activitySpy = sinon.spy();
      fs.emptyDirSync(path.resolve("tests/test-data/targets"));
      execSync(`echo 'this wont match' > ${testtarget_3}`);
      downloadOne("/testtarget_1", testtarget_1)
        .then(thenSpy)
        .catch(catchSpy);

      const fakeRes = {
        status: 200,
        headers: {
          "content-type": "application/octet-stream",
          "content-length": 1
        },
        response: {
          pipe: writer => setTimeout(() => writer.emit("finish"), 50),
          on: (event, cb) => setTimeout(() => cb(["a"]), 50)
        }
      };

      moxios.wait(function() {
        moxios.requests
          .at(0)
          .respondWith(fakeRes)
          .then(function() {
            try {
              expect(catchSpy).to.not.have.been.called;
              expect(thenSpy).to.have.been.calledOnce;
              expect(thenSpy).to.not.have.been.calledTwice;
              done();
            } catch (e) {
              done(`unfulfilled assertions: ${e}`);
            }
          });
      });
    });
  });
  describe("download()", function() {
    it("should download missing files", function(done) {
      const thenSpy = sinon.spy();
      const catchSpy = sinon.fake((...e) => console.log(...e));
      const progressSpy = sinon.spy();
      const nextSpy = sinon.spy();
      const activitySpy = sinon.spy();
      fs.emptyDirSync(path.resolve("tests/test-data/targets"));
      execSync(`echo 'this wont match' > ${testtarget_3}`);
      download(
        [
          {
            url: "/testfile",
            path: testfile,
            checksum: {
              algorithm: "sha256",
              sum
            }
          },
          {
            url: "/testtarget_1",
            path: testtarget_1,
            checksum: {
              algorithm: "md5",
              sum: "d41d8cd98f00b204e9800998ecf8427e"
            }
          },
          {
            url: "/testtarget_2",
            path: testtarget_2
          },
          {
            url: "/testtarget_3",
            path: testtarget_3,
            checksum: {
              algorithm: "md5",
              sum: "d41d8cd98f00b204e9800998ecf8427e"
            }
          }
        ],
        progressSpy,
        nextSpy,
        activitySpy
      )
        .then(thenSpy)
        .catch(catchSpy);

      const fakeRes = {
        status: 200,
        headers: {
          "content-type": "application/octet-stream",
          "content-length": 1
        },
        response: {
          pipe: writer => setTimeout(() => writer.emit("finish"), 50),
          on: (event, cb) => setTimeout(() => cb(["a"]), 50)
        }
      };

      moxios.wait(function() {
        moxios.requests
          .at(0)
          .respondWith(fakeRes)
          .then(() => moxios.requests.at(1).respondWith(fakeRes))
          .then(() => moxios.requests.at(2).respondWith(fakeRes))
          .then(function() {
            try {
              expect(catchSpy).to.not.have.been.called;
              expect(thenSpy).to.have.been.calledOnce;
              expect(thenSpy).to.not.have.been.calledTwice;
              expect(activitySpy).to.have.been.calledWith("preparing");
              expect(activitySpy).to.have.been.calledWith("downloading");
              expect(nextSpy).to.have.been.calledWith(0, 3);
              expect(nextSpy).to.have.been.calledWith(1, 3);
              expect(nextSpy).to.have.been.calledWith(2, 3);
              expect(nextSpy).to.have.been.calledWith(3, 3);
              done();
            } catch (e) {
              done(`unfulfilled assertions: ${e}`);
            }
          });
      });
    });

    it("should not download if all files matched", function(done) {
      const progressSpy = sinon.spy();
      const nextSpy = sinon.spy();
      const activitySpy = sinon.spy();
      fs.emptyDirSync(path.resolve("tests/test-data/targets"));
      execSync(`echo 'this wont match' > ${testtarget_3}`);
      download(
        [
          {
            url: "/testfile",
            path: testfile,
            checksum: {
              algorithm: "sha256",
              sum
            }
          }
        ],
        progressSpy,
        nextSpy,
        activitySpy
      )
        .then(() => {
          expect(activitySpy).to.have.been.calledWith("preparing");
          expect(nextSpy).to.not.have.been.called;
          expect(progressSpy).to.not.have.been.called;
          done();
        })
        .catch(done);
    });
    it("should reject on mismatch after download", function(done) {
      const thenSpy = sinon.spy();
      const catchSpy = sinon.spy();
      const progressSpy = sinon.spy();
      const nextSpy = sinon.spy();
      const activitySpy = sinon.spy();
      fs.emptyDirSync(path.resolve("tests/test-data/targets"));
      download(
        [
          {
            url: "/testtarget_1",
            path: testtarget_1,
            checksum: {
              algorithm: "sha256",
              sum: "nope"
            }
          }
        ],
        progressSpy,
        nextSpy,
        activitySpy
      )
        .then(thenSpy)
        .catch(catchSpy);

      const fakeRes = {
        status: 200,
        headers: {
          "content-type": "application/octet-stream",
          "content-length": 1
        },
        response: {
          pipe: writer => setTimeout(() => writer.emit("finish"), 50),
          on: (event, cb) => setTimeout(() => cb(["a"]), 50)
        }
      };

      moxios.wait(function() {
        moxios.requests
          .at(0)
          .respondWith(fakeRes)
          .then(function() {
            try {
              expect(thenSpy).to.not.have.been.called;
              expect(thenSpy).to.not.have.been.calledTwice;
              expect(catchSpy).to.have.been.called;
              expect(catchSpy).to.not.have.been.calledTwice;
              expect(activitySpy).to.have.been.calledWith("preparing");
              expect(activitySpy).to.have.been.calledWith("downloading");
              expect(nextSpy).to.have.been.calledWith(0, 1);
              expect(nextSpy).to.have.been.calledWith(1, 1);
              done();
            } catch (e) {
              done(`unfulfilled assertions: ${e}`);
            }
          });
      });
    });
    it("should reject on download error", function(done) {
      const thenSpy = sinon.spy();
      const catchSpy = sinon.spy();
      fs.emptyDirSync(path.resolve("tests/test-data/targets"));
      download([
        {
          url: "/testtarget_1",
          path: testtarget_1
        }
      ])
        .then(thenSpy)
        .catch(catchSpy);

      const fakeRes = {
        status: 500,
        headers: {
          "content-type": "application/octet-stream",
          "content-length": 1
        },
        response: {
          pipe: writer => setTimeout(() => writer.emit("error"), 50),
          on: (event, cb) => setTimeout(() => cb(["a"]), 50)
        }
      };

      moxios.wait(function() {
        moxios.requests
          .at(0)
          .respondWith(fakeRes)
          .then(function() {
            try {
              expect(thenSpy).to.not.have.been.called;
              expect(thenSpy).to.not.have.been.calledTwice;
              expect(catchSpy).to.have.been.called;
              expect(catchSpy).to.not.have.been.calledTwice;
              done();
            } catch (e) {
              done(`unfulfilled assertions: ${e}`);
            }
          });
      });
    });
  });
});
